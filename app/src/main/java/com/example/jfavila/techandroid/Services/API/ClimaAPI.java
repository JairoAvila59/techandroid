package com.example.jfavila.techandroid.Services.API;

import com.example.jfavila.techandroid.Services.API.model.Feed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by jfavila on 29/01/2018.
 */

public interface ClimaAPI {

    String BASE_URL = "https://api.darksky.net/forecast/";

    @Headers("Content-Type: application/json")
    @GET("ea76e78f539ef7dae1879fd1a45d3628/{coord}")
    Call<Feed> funcionPrueba(@Path("coord") String coord);
}

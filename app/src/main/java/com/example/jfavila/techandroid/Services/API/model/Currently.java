package com.example.jfavila.techandroid.Services.API.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;

/**
 * Created by jfavila on 29/01/2018.
 */

public class Currently{

        @SerializedName("summary")
        @Expose
        private String summary;

        @SerializedName("icon")
        @Expose
        private String icon;

        @SerializedName("precipProbability")
        @Expose
        private double precipProbability;

        @SerializedName("temperature")
        @Expose
        private double temperature;

        @SerializedName("humidity")
        @Expose
        private double humidity;


        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getIcon() {
            return icon;
        }


        public void setIcon(String icon) {
            this.icon = icon;
        }

        public double getPrecipProbability() {
            return precipProbability;
        }

        public void setPrecipProbability(double precipProbability) {
            this.precipProbability = precipProbability;
        }

        public double getTemperature() {
            temperature = (5*(temperature-32))/9;
            DecimalFormat df = new DecimalFormat("#.0");
            return Double.parseDouble(df.format(temperature));
        }

        public void setTemperature(double temperature) {
            this.temperature = temperature;
        }

        public double getHumidity() {
            return humidity;
        }


        public void setHumidity(double humidity) {
            this.humidity = humidity;
        }

        @Override
        public String toString() {
            return "Currently{" +
                    "summary='" + summary + '\'' +
                    ", icon='" + icon + '\'' +
                    ", precipProbability=" + precipProbability +
                    ", temperature=" + temperature +
                    ", humidity=" + humidity +
                    '}';
        }

}

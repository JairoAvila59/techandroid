package com.example.jfavila.techandroid.Model;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.example.jfavila.techandroid.GPS;
import com.example.jfavila.techandroid.Services.API.ClimaAPI;
import com.example.jfavila.techandroid.Services.API.model.Currently;
import com.example.jfavila.techandroid.Services.API.model.Feed;
import com.example.jfavila.techandroid.Services.GPS.GpsTracker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jfavila on 27/01/2018.
 */

public class GPSModel implements GPS.Model {

    private GPS.Presenter presenter;
    public String lat;
    public String lng;
    public String zona;
    public String summary;
    public String icon;
    public double precip;
    public double temperature;
    public double humidity;
    public String msg;


    public GPSModel(GPS.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void coordenada(Boolean data, Context context) {
        if(data){
            String cadena;
            GpsTracker gt = new GpsTracker(context);
            Location l = gt.getLocation();
            if( l != null){
                double lat_number = l.getLatitude();
                double lon_number = l.getLongitude();
                lat = String.valueOf(lat_number);
                lng = String.valueOf(lon_number);
                cadena = lat+", "+lng;
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ClimaAPI.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                ClimaAPI climaAPI = retrofit.create(ClimaAPI.class);

                Call<Feed> call = climaAPI.funcionPrueba(cadena);

                Log.d("error", "onFailure: Something went wrong: " + call.request().url() );

                call.enqueue(new Callback<Feed>() {
                    @Override
                    public void onResponse(Call<Feed> call, Response<Feed> response) {
                        zona = response.body().getTimezone();
                        Currently currently = response.body().getCurrently();
                        summary = currently.getSummary();
                        icon = currently.getIcon();
                        precip = currently.getPrecipProbability();
                        temperature = currently.getTemperature();
                        humidity = currently.getHumidity();
                        msg = null;
                        presenter.showResult(zona, summary, icon, msg, precip, temperature, humidity);
                    }

                    @Override
                    public void onFailure(Call<Feed> call, Throwable t) {
                        Log.d("error", "onFailure: Something went wrong: " + t.getMessage() );
                    }
                });

            }else {
                presenter.showResult(null, null,null, "No se ha detectado GPS", 0, 0, 0);
            }

        }
    }
}

package com.example.jfavila.techandroid;

import android.content.Context;

/**
 * Created by jfavila on 27/01/2018.
 */

public interface GPS {

    interface  View{
        void showResult(String zona, String summary, String icon, String msg, double precip, double temperature, double humidity);
    }

    interface  Presenter{
        void showResult(String zona, String summary, String icon, String msg, double precip, double temperature, double humidity);
        void coordenada(Boolean data, Context context);
    }

    interface  Model{
        void coordenada(Boolean data, Context context);
    }
}

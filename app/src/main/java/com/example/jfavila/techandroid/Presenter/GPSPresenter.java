package com.example.jfavila.techandroid.Presenter;

import android.content.Context;

import com.example.jfavila.techandroid.GPS;
import com.example.jfavila.techandroid.Model.GPSModel;

/**
 * Created by jfavila on 27/01/2018.
 */

public class GPSPresenter implements GPS.Presenter {

    private GPS.View view;
    private GPS.Model model;

    public GPSPresenter(GPS.View view){
        this.view = view;
        model = new GPSModel(this);
    }

    @Override
    public void showResult(String zona, String summary, String icon, String msg, double precip, double temperature, double humidity) {
        if(view!=null){
            view.showResult(zona,summary,icon,msg,precip,temperature,humidity);
        }
    }

    @Override
    public void coordenada(Boolean data, Context context) {
        if(view!=null)
        {
            model.coordenada(data, context);
        }

    }
}

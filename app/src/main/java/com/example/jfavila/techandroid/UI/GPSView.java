package com.example.jfavila.techandroid.UI;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jfavila.techandroid.GPS;
import com.example.jfavila.techandroid.Presenter.GPSPresenter;
import com.example.jfavila.techandroid.R;

import java.text.DecimalFormat;

public class GPSView extends AppCompatActivity implements GPS.View {

    private Button btnLoc;
    private GPS.Presenter presenter;
    private TextView zone;
    private TextView icono;
    private TextView precipitacion;
    private TextView temperatura;
    private TextView humedad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLoc = (Button) findViewById(R.id.btnGetLoc);
        zone = (TextView) findViewById(R.id.zone);
        icono = (TextView) findViewById(R.id.icono);
        precipitacion = (TextView) findViewById(R.id.precip);
        temperatura = (TextView) findViewById(R.id.weather_icon);
        humedad = (TextView) findViewById(R.id.humidity);
        presenter = new GPSPresenter(this);
        ActivityCompat.requestPermissions(GPSView.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        presenter.coordenada(true, getApplicationContext());

    }

    public void calcular (View view){

        presenter.coordenada(true, getApplicationContext());
    }

    @Override
    public void showResult(String zona, String summary, String icon, String msg, double precip, double temperature, double humidity) {
        if(msg == null) {
            zone.setText(zona);
            icono.setText("Resumen: " + summary);
            precipitacion.setText("Precipitacion: " + String.valueOf(precip) + " mm");
            temperatura.setText(String.valueOf(temperature) + " °C");
            humedad.setText("Humedad: " + String.valueOf(humidity) + " %");
        }else {
            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
        }
    }
}

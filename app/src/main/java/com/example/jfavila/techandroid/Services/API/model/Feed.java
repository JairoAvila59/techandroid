package com.example.jfavila.techandroid.Services.API.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jfavila on 29/01/2018.
 */

public class Feed {

    @SerializedName("timezone")
    @Expose
    private String timezone;

    @SerializedName("currently")
    @Expose
    private Currently currently;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Currently getCurrently() {
        return currently;
    }


    public void setCurrently(Currently currently) {
        this.currently = currently;
    }

    @Override
    public String toString() {
        return "Feed{" +
                "timezone='" + timezone + '\'' +
                ", currently=" + currently +
                '}';
    }
}
